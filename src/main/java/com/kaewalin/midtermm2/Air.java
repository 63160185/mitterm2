/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.midtermm2;

/**
 *
 * @author ACER
 */
public class Air {

    //ประกาศ Class ตัวแปร
    protected int wide; //เก็บค่าตัวแปรความกว้าง
    protected int _long; //เก็บค่าตัวแปรความยาว
    protected int area; //เก็บค่าตัวแปรพื้นที่
    protected int BTU; //เก็บค่าตัวแปร British Thermal Unit   

    public Air(int wide, int _long) {
        this.wide = wide;
        this._long = _long;

    }

    public void Area() {
        this.area = this.wide * this._long;
        

    }
    public void CalBTU() { //Overriding Class แม่

    }
    public void ShowProduce() {
        //แสดงข้อความ ความกว้าง ความยาว พื้นที่ และ British Thermal Unit ที่เหมาะสมสำหรับอุณหภูมิห้อง      
        System.out.println("Your Room Wide : " + this.wide + " Long : " + this._long + " Area Of Your Room Is : " + this.area + " BTU Appropriate Is : " + this.BTU);
    }

}
