/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.midtermm2;

/**
 *
 * @author ACER
 */
public class TestAir {
    public static void main(String[] args) {
        Office kaew = new Office(3, 4);
        System.out.println("Office Number 1");
        kaew.Area();
        kaew.ShowProduce();
        kaew.ShowProduce("BTU");
        kaew.ShowProduce("AREA");
        System.out.println("---------------------------------------");
        
        Office tuch = new Office(7, 4);
        System.out.println("Office Number 2");
        tuch.Area();
        tuch.ShowProduce();
        tuch.ShowProduce("BTU");
        tuch.ShowProduce("AREA");
        System.out.println("---------------------------------------");
        
        Office tata = new Office(10, 2);
        System.out.println("Office Number 3");
        tata.Area();
        tata.ShowProduce();
        tata.ShowProduce("BTU");
        tata.ShowProduce("AREA");
        System.out.println("---------------------------------------");
        
        BedRoom b = new BedRoom(10, 0);
        System.out.println("BedRoom");
        b.Area();
        b.ShowProduce();
        b.ShowProduce("BTU");
        b.ShowProduce("AREA");

        
    }
    
}
