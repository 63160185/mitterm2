/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.midtermm2;

/**
 *
 * @author ACER
 */
public class Office extends Air {

    public Office(int wide, int _long) {
        super(wide, _long);

    }

    public void CalBTU() { //Overriding Class ลูก
        if (area >= 4 && area <= 8) {
            BTU = 5000;
        } else if (area >= 9 && area <= 13) {
            BTU = 9000;
        } else if (area >= 14 && area <= 16) {
            BTU = 12000;
        } else if (area >= 17 && area <= 21) {
            BTU = 15000;
        } else if (area >= 22 && area <= 26) {
            BTU = 18000;
        } else if (area >= 27 && area <= 31) {
            BTU = 20000;
        } else if (area >= 32 && area <= 36) {
            BTU = 22000;
        } else if (area >= 37 && area <= 41) {
            BTU = 24000;
        } else if (area >= 42 && area <= 46) {
            BTU = 30000;

        } else {
            BTU = 0;

        }

    }

    public boolean Requirmant() {
        CalBTU();
        if (BTU == 0) {
        } else {
            return true;
        }
        return false;

    }

    public void ShowProduct() {
        if (Requirmant() == true) {
            System.out.println("Your OfficeRoom Wide : " + this.wide + " Long : " + this._long + " Area Of Your Room Is : " + this.area + " BTU Appropriate Is : " + this.BTU);
        } else {
            System.out.println("Your OfficeRoom Wide : " + this.wide + " Long : " + this._long + " Area Of Your Room Is : " + this.area);
            System.out.println("No BTU to your room size. ");
        }
    }

    public void ShowProduct(String required) { //Overload จาก Method ShowProduct จะเห็นได้ว่าเงื่อนไขต่างกันแต่ยังใช้ชื่อ Method ตัวเดียวกัน
        if (Requirmant() == true) {
            if (required.equals("BTU")) {
                System.out.println("BTU Appropriate Is : " + this.BTU);

            } else if (required.equals("AREA")) {
                System.out.println(" Area Is : " + area);

            }
        }

    }

}
