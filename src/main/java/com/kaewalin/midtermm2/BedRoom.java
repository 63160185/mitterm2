/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.midtermm2;

/**
 *
 * @author ACER
 */
public class BedRoom extends Air {

    public BedRoom(int wide, int _long) {
        super(wide, _long);

    }

    public void CalBTU() { //Overriding Class ลูก
        if (area >= 5 && area <= 9) {
            BTU = 5000;
        } else if (area >= 10 && area <= 14) {
            BTU = 9000;
        } else if (area >= 15 && area <= 19) {
            BTU = 12000;
        } else if (area >= 20 && area <= 24) {
            BTU = 15000;
        } else if (area >= 25 && area <= 29) {
            BTU = 18000;
        } else if (area >= 30 && area <= 34) {
            BTU = 20000;
        } else if (area >= 35 && area <= 39) {
            BTU = 22000;
        } else if (area >= 40 && area <= 44) {
            BTU = 24000;
        } else if (area >= 45 && area <= 50) {
            BTU = 30000;

        } else {
            BTU = 0;
        }

    }

    public boolean Requirmant() {
        CalBTU();
        if (BTU == 0) {
        } else {
            return true;
        }
        return false;

    }

    public void ShowProduct() {
        if (Requirmant() == true) {
            System.out.println("Your BedRoom Wide : " + this.wide + " Long : " + this._long + " Area Of Your Room Is : " + this.area + " BTU Appropriate Is : " + this.BTU);
        } else {
            System.out.println("Your BedRoom Wide : " + this.wide + " Long : " + this._long + " Area Of Your Room Is : " + this.area);
            System.out.println("No BTU to your room size. ");
        }
    }

    public void ShowProduct(String required) {
        if (Requirmant() == true) {
            if (required.equals("BTU")) {
                System.out.println("BTU Appropriate Is : " + this.BTU);

            } else if (required.equals("AREA")) {
                System.out.println(" Area Is : " + area);

            }
        }

    }

}
